package com.tousled.theagilemonkeys.converters;

import com.tousled.theagilemonkeys.CustomerObjectMother;
import com.tousled.theagilemonkeys.CustomerResponseObjectMother;
import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.models.dto.CustomerResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomerToCustomerResponseConverterTest {

    private CustomerToCustomerResponseConverter cut;

    @BeforeEach
    void setUp() {
        this.cut = new CustomerToCustomerResponseConverter();
    }

    @Test
    void itShouldConvertFromCustomerToCustomerResponse() {
        Customer source = CustomerObjectMother
                .createCustomer(UUID.randomUUID(), "John", "Lenon", "photo-path", "john.lenon@monkeys.com", "created_by");
        CustomerResponse expectedResponse = CustomerResponseObjectMother.createCustomerResponseFromCustomer(source);
        CustomerResponse actualResponse = this.cut.convert(source);

        assertEquals(expectedResponse, actualResponse);
        assertEquals(expectedResponse.getCreatedBy(), actualResponse.getCreatedBy());
        assertEquals(expectedResponse.getModifiedBy(), actualResponse.getModifiedBy());
    }
}