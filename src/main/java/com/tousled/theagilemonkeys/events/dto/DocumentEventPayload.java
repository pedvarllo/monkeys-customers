package com.tousled.theagilemonkeys.events.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Tolerate;

import java.util.UUID;

@Data
@AllArgsConstructor
@Builder
@ToString
public class DocumentEventPayload {

    private UUID id;
    private UUID ownerId;
    private String userId;
    private String documentPath;

    @Tolerate
    public DocumentEventPayload() {
    }
}
