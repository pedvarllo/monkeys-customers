package com.tousled.theagilemonkeys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class TheAgileMonkeysApplication {

    public static void main(String[] args) {
        SpringApplication.run(TheAgileMonkeysApplication.class, args);
    }

    @Bean
    OAuth2ClientProperties oAuth2ClientProperties() {
        return new OAuth2ClientProperties();
    }

}
