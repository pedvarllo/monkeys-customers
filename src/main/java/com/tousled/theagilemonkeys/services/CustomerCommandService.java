package com.tousled.theagilemonkeys.services;

import com.tousled.theagilemonkeys.exceptions.BusinessException;
import com.tousled.theagilemonkeys.exceptions.EntityNotFoundException;
import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.repositories.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class CustomerCommandService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerCommandService.class);
    private static final String CUSTOMER_NOT_FOUND_ERROR_MESSAGE = "Customer with id %s not found";
    private static final String EMAIL_ALREADY_REGISTERED_ERROR_MESSAGE = "The email %s already exists. Choose another one";

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerCommandService(
            CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public UUID createCustomer(Customer customer, String userId) {

        if (customerRepository.findByEmail(customer.getEmail()).isPresent()) {
            LOGGER.error("The email {} already exists", customer.getEmail());
            throw new BusinessException(String.format(EMAIL_ALREADY_REGISTERED_ERROR_MESSAGE, customer.getEmail()));
        }

        LOGGER.info("Saving customer in database");
        customer.setCreatedBy(userId);
        customer.setCreationDateTime(LocalDateTime.now());
        Customer createdCustomer = this.customerRepository.save(customer);
        return createdCustomer.getId();
    }

    public void updateCustomerDocument(String photoPath, UUID customerId, String userId) {
        Optional<Customer> optionalCustomer = this.customerRepository.findById(customerId);

        if (optionalCustomer.isEmpty()) {
            LOGGER.error("The customer with id {} does not exist", customerId);
            throw new BusinessException(String.format(CUSTOMER_NOT_FOUND_ERROR_MESSAGE, customerId));
        }

        LOGGER.info("Updating customer document");
        Customer customer = optionalCustomer.get();
        customer.setPhotoPath(photoPath);
        customer.setModifiedBy(userId);
        customer.setModificationDateTime(LocalDateTime.now());
        this.customerRepository.save(customer);

    }

    public void updateCustomer(Customer customer, String userId) throws EntityNotFoundException {

        Optional<Customer> optionalCustomer = customerRepository.findById(customer.getId());

        if (optionalCustomer.isEmpty()) {
            LOGGER.error("The customer with id {} does not exist", customer.getId());
            throw new EntityNotFoundException(String.format(CUSTOMER_NOT_FOUND_ERROR_MESSAGE, customer.getId()));
        }

        Customer oldCustomer = optionalCustomer.get();
        oldCustomer.setName(customer.getName());
        oldCustomer.setSurname(customer.getSurname());

        oldCustomer.setModifiedBy(userId);
        oldCustomer.setModificationDateTime(LocalDateTime.now());

        this.customerRepository.save(oldCustomer);
    }

    public void deleteCustomer(UUID customerId) {

        Optional<Customer> optionalCustomer = customerRepository.findById(customerId);

        if (optionalCustomer.isEmpty()) {
            LOGGER.error("The customer with id {} does not exist", customerId);
            throw new EntityNotFoundException(String.format(CUSTOMER_NOT_FOUND_ERROR_MESSAGE, customerId));
        }

        customerRepository.delete(optionalCustomer.get());

    }
}
