package com.tousled.theagilemonkeys;

import com.tousled.theagilemonkeys.models.dto.CustomerUpdateCommand;

import java.util.UUID;

public class CustomerCommandUpdateObjectMother {

    public static CustomerUpdateCommand createCustomerUpdateCommand(String name, String surname) {
        return CustomerUpdateCommand.builder()
                .id(UUID.randomUUID())
                .name(name)
                .surname(surname)
                .build();
    }
}
