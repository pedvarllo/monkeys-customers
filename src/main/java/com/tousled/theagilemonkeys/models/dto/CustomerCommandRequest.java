package com.tousled.theagilemonkeys.models.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerCommandRequest {

    private String name;
    private String surname;
    private String email;

}
