package com.tousled.theagilemonkeys;

import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.models.dto.CustomerFullDetailResponse;
import com.tousled.theagilemonkeys.models.dto.CustomerResponse;

import java.util.List;
import java.util.UUID;

public class CustomerResponseObjectMother {

    public static List<CustomerResponse> createCustomerResponseList() {
        return List.of(
                CustomerResponse.builder()
                        .id(UUID.fromString("02ab5dfa-47fd-4dc9-ab09-cc11bf0d411d"))
                        .name("John")
                        .surname("Lipa")
                        .email("john.lipa@monkeys.com")
                        .build()
        );
    }

    public static CustomerResponse createCustomerResponseFromCustomer(Customer customer) {
        return CustomerResponse.builder()
                .id(customer.getId())
                .name(customer.getName())
                .surname(customer.getSurname())
                .email(customer.getEmail())
                .createdBy(customer.getCreatedBy())
                .build();
    }

    public static CustomerFullDetailResponse createCustomerFullDetailResponseFromCustomer(Customer customer) {
        return CustomerFullDetailResponse.builder()
                .id(customer.getId())
                .name(customer.getName())
                .surname(customer.getSurname())
                .email(customer.getEmail())
                .createdBy(customer.getCreatedBy())
                .modifiedBy(customer.getModifiedBy())
                .creationDateTime(customer.getCreationDateTime())
                .modificationDateTime(customer.getModificationDateTime())
                .build();
    }
}
