package com.tousled.theagilemonkeys.events;

import com.tousled.theagilemonkeys.events.dto.DocumentEventPayload;
import com.tousled.theagilemonkeys.services.CustomerCommandService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.support.Acknowledgment;

import java.util.UUID;

import static org.mockito.Mockito.*;

class KafkaConsumerTest {

    private Acknowledgment acknowledgment;
    private CustomerCommandService customerCommandService;
    private KafkaConsumer cut;

    @BeforeEach
    void setUp() {
        this.acknowledgment = mock(Acknowledgment.class);
        this.customerCommandService = mock(CustomerCommandService.class);
        this.cut = new KafkaConsumer(customerCommandService);
    }

    @Test
    void itShouldCallCommandServiceIfPayloadIsNotNull() {

        DocumentEventPayload documentEventPayload = DocumentEventPayload.builder()
                .documentPath("document-path")
                .userId("user-id")
                .ownerId(UUID.randomUUID())
                .build();

        this.cut.consumeEvents(documentEventPayload, acknowledgment);

        verify(this.customerCommandService)
                .updateCustomerDocument(documentEventPayload.getDocumentPath(), documentEventPayload.getOwnerId(), documentEventPayload.getUserId());
        verify(acknowledgment).acknowledge();
    }

    @Test
    void itShouldNotCallCommandServiceIfPayloadIsNull() {

        this.cut.consumeEvents(null, acknowledgment);

        verifyNoInteractions(customerCommandService);
        verify(acknowledgment).acknowledge();
    }
}