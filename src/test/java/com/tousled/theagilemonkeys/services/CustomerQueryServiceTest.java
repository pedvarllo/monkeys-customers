package com.tousled.theagilemonkeys.services;

import com.tousled.theagilemonkeys.CustomerObjectMother;
import com.tousled.theagilemonkeys.exceptions.EntityNotFoundException;
import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.repositories.CustomerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CustomerQueryServiceTest {

    private static final String CUSTOMER_NOT_FOUND_ERROR_MESSAGE = "Customer with id %s not found";

    private CustomerRepository customerRepository;
    private CustomerQueryService sut;

    @BeforeEach
    void setUp() {
        this.customerRepository = mock(CustomerRepository.class);
        this.sut = new CustomerQueryService(customerRepository);
    }

    @Test
    void itShouldGetAllCustomers() {

        List<Customer> expectedResponse = CustomerObjectMother.createCustomerList();
        when(customerRepository.findAll()).thenReturn(expectedResponse);

        List<Customer> actualResponse = this.sut.getAllCustomers();

        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    void itShouldGetCustomerWithId() {

        UUID id = UUID.randomUUID();
        Customer expectedResponse = CustomerObjectMother
                .createCustomer(id, "Dua", "Lipa", "photo-url", "dua.lipa@monkeys.com", "created-by");

        when(customerRepository.findById(id)).thenReturn(Optional.of(expectedResponse));

        Customer actualResponse = this.sut.getCustomerById(id);

        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    void itShouldThrowNotFoundExceptionIfCustomerIsNotFound() {

        UUID id = UUID.randomUUID();

        when(customerRepository.findById(id)).thenReturn(Optional.empty());

        Exception exception = assertThrows(Exception.class, () -> this.sut.getCustomerById(id));

        assertEquals(EntityNotFoundException.class, exception.getClass());
        assertEquals(String.format(CUSTOMER_NOT_FOUND_ERROR_MESSAGE, id), exception.getMessage());
    }
}