package com.tousled.theagilemonkeys.converters;

import com.tousled.theagilemonkeys.CustomerCommandUpdateObjectMother;
import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.models.dto.CustomerUpdateCommand;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomerUpdateCommandToCustomerConverterTest {

    private CustomerUpdateCommandToCustomerConverter cut;

    @BeforeEach
    void setUp() {
        this.cut = new CustomerUpdateCommandToCustomerConverter();
    }

    @Test
    void itShouldConvertFromCustomerUpdateCommandToCustomer() {
        CustomerUpdateCommand source = CustomerCommandUpdateObjectMother.createCustomerUpdateCommand("Name", "surname");

        Customer actual = this.cut.convert(source);

        assertEquals(source.getId(), actual.getId());
        assertEquals(source.getName(), actual.getName());
        assertEquals(source.getSurname(), actual.getSurname());
    }
}