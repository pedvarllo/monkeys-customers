package com.tousled.theagilemonkeys.converters;

import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.models.dto.CustomerUpdateCommand;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerUpdateCommandToCustomerConverter implements Converter<CustomerUpdateCommand, Customer> {

    @Override
    public Customer convert(CustomerUpdateCommand source) {
        return Customer.builder()
                .id(source.getId())
                .name(source.getName())
                .surname(source.getSurname())
                .build();
    }
}
