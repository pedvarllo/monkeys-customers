package com.tousled.theagilemonkeys.services;

import com.tousled.theagilemonkeys.exceptions.EntityNotFoundException;
import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CustomerQueryService {

    private static final String CUSTOMER_NOT_FOUND_ERROR_MESSAGE = "Customer with id %s not found";

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerQueryService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    public Customer getCustomerById(UUID id) {
        Optional<Customer> optionalCustomer = customerRepository.findById(id);
        if (optionalCustomer.isPresent()) {
            return optionalCustomer.get();
        }

        throw new EntityNotFoundException(String.format(CUSTOMER_NOT_FOUND_ERROR_MESSAGE, id));

    }
}
