package com.tousled.theagilemonkeys.models.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.UUID;

@Data
@Builder
public class CustomerResponse {

    private UUID id;
    private String name;
    private String surname;
    private String createdBy;
    private String modifiedBy;
    private String email;

    @Tolerate
    public CustomerResponse() {
    }

}
