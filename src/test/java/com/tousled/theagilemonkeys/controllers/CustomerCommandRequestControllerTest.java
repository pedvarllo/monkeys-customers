package com.tousled.theagilemonkeys.controllers;

import com.tousled.theagilemonkeys.CustomerCommandObjectMother;
import com.tousled.theagilemonkeys.CustomerCommandUpdateObjectMother;
import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.models.dto.CustomerCommandRequest;
import com.tousled.theagilemonkeys.models.dto.CustomerCommandResponse;
import com.tousled.theagilemonkeys.models.dto.CustomerUpdateCommand;
import com.tousled.theagilemonkeys.services.CustomerCommandService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class CustomerCommandRequestControllerTest {

    private static final String X_AUTH_USER_ID_HEADER = "x-auth-user-id";

    private CustomerCommandService customerCommandService;
    private ConversionService conversionService;
    private CustomerCommandController cut;

    @BeforeEach
    void setUp() {
        this.customerCommandService = mock(CustomerCommandService.class);
        this.conversionService = mock(ConversionService.class);
        this.cut = new CustomerCommandController(customerCommandService, conversionService);
    }

    @Test
    void itShouldReturnCreationOkNoContent() {

        CustomerCommandRequest customerCommandRequest = CustomerCommandObjectMother.createCustomerCommand("Dua", "Lipa", "dua.lipa@monkeys.com");
        Customer mockCustomer = Customer.builder().build();
        CustomerCommandResponse expectedResponse = CustomerCommandResponse.builder().customerId(UUID.randomUUID()).build();

        when(conversionService.convert(eq(customerCommandRequest), eq(Customer.class))).thenReturn(mockCustomer);
        when(customerCommandService.createCustomer(any(Customer.class), anyString())).thenReturn(expectedResponse.getCustomerId());

        ResponseEntity<CustomerCommandResponse> actualResponse = this.cut.createCustomer(customerCommandRequest, X_AUTH_USER_ID_HEADER);

        verify(customerCommandService).createCustomer(mockCustomer, X_AUTH_USER_ID_HEADER);
        assertEquals(HttpStatus.CREATED, actualResponse.getStatusCode());
        assertEquals(expectedResponse, actualResponse.getBody());

    }

    @Test
    void itShouldReturnUpdatedOkNoContent() {

        CustomerUpdateCommand customerUpdateCommand = CustomerCommandUpdateObjectMother
                .createCustomerUpdateCommand("Dua", "Lipa");
        Customer mockCustomer = Customer.builder().build();

        when(conversionService.convert(eq(customerUpdateCommand), eq(Customer.class))).thenReturn(mockCustomer);
        doNothing().when(customerCommandService).updateCustomer(any(Customer.class), anyString());

        ResponseEntity<Void> actualResponse = this.cut.updateCustomer(customerUpdateCommand, X_AUTH_USER_ID_HEADER);

        assertEquals(HttpStatus.NO_CONTENT, actualResponse.getStatusCode());

    }

    @Test
    void itShouldReturnDeleteOkNoContent() {

        UUID customerIdToDelete = UUID.randomUUID();
        ResponseEntity<Void> actualResponse = this.cut.deleteCustomer(customerIdToDelete);

        verify(customerCommandService).deleteCustomer(customerIdToDelete);
        assertEquals(HttpStatus.NO_CONTENT, actualResponse.getStatusCode());

    }


}