package com.tousled.theagilemonkeys;

import com.tousled.theagilemonkeys.models.dto.CustomerCommandRequest;

public class CustomerCommandObjectMother {

    public static CustomerCommandRequest createCustomerCommand(String name, String surname, String email) {
        return CustomerCommandRequest.builder()
                .name(name)
                .surname(surname)
                .email(email)
                .build();
    }
}
