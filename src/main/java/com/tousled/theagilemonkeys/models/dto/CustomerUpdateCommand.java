package com.tousled.theagilemonkeys.models.dto;


import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class CustomerUpdateCommand {

    private UUID id;
    private String name;
    private String surname;
}