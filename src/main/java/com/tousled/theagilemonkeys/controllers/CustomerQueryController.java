package com.tousled.theagilemonkeys.controllers;

import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.models.dto.CustomerFullDetailResponse;
import com.tousled.theagilemonkeys.models.dto.CustomerResponse;
import com.tousled.theagilemonkeys.services.CustomerQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerQueryController {

    private final CustomerQueryService customerQueryService;
    private final ConversionService conversionService;

    @Autowired
    public CustomerQueryController(CustomerQueryService customerQueryService, ConversionService conversionService) {
        this.customerQueryService = customerQueryService;
        this.conversionService = conversionService;
    }

    @GetMapping
    public ResponseEntity<List<CustomerResponse>> getAllCustomers() {
        List<Customer> customerList = this.customerQueryService.getAllCustomers();
        List<CustomerResponse> response = customerList.stream()
                .map(c -> this.conversionService.convert(c, CustomerResponse.class))
                .collect(Collectors.toList());
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerFullDetailResponse> getCustomerById(@PathVariable("id") UUID id) {
        CustomerFullDetailResponse customer = this.conversionService.convert(customerQueryService.getCustomerById(id), CustomerFullDetailResponse.class);
        return ResponseEntity.ok(customer);
    }
}
