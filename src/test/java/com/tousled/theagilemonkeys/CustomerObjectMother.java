package com.tousled.theagilemonkeys;

import com.tousled.theagilemonkeys.models.Customer;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public class CustomerObjectMother {

    public static Customer createCustomer(UUID id, String name, String surname, String photoPath, String email, String createdBy) {
        return Customer.builder()
                .id(id)
                .name(name)
                .surname(surname)
                .photoPath(photoPath)
                .email(email)
                .createdBy(createdBy)
                .build();
    }

    public static Customer createFullCustomer(UUID id, String name, String surname, String photoPath, String email, String createdBy) {
        return Customer.builder()
                .id(id)
                .name(name)
                .surname(surname)
                .photoPath(photoPath)
                .email(email)
                .createdBy(createdBy)
                .creationDateTime(LocalDateTime.now())
                .modificationDateTime(LocalDateTime.now())
                .build();
    }

    public static List<Customer> createCustomerList() {
        return List.of(
                Customer.builder()
                        .id(UUID.fromString("02ab5dfa-47fd-4dc9-ab09-cc11bf0d411d"))
                        .name("John")
                        .surname("Lipa")
                        .photoPath("photo-path")
                        .email("john.lipa@monkeys.com")
                        .build()
        );
    }

}
