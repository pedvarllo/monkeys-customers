package com.tousled.theagilemonkeys.services;

import com.tousled.theagilemonkeys.exceptions.BusinessException;
import com.tousled.theagilemonkeys.exceptions.EntityNotFoundException;
import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.repositories.CustomerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class CustomerCommandRequestServiceTest {

    private static final String CUSTOMER_NOT_FOUND_ERROR_MESSAGE = "Customer with id %s not found";
    private static final String EMAIL_ALREADY_REGISTERED_ERROR_MESSAGE = "The email %s already exists. Choose another one";

    private CustomerRepository customerRepository;
    private CustomerCommandService sut;

    @BeforeEach
    void setUp() {
        this.customerRepository = mock(CustomerRepository.class);
        this.sut = new CustomerCommandService(customerRepository);
    }

    @Test
    void itShouldSaveCustomerIfTheEmailDoesNotExist() {
        Customer customer = mock(Customer.class);
        String userId = "x-auth-user-id";
        UUID customerId = UUID.randomUUID();

        when(customer.getId()).thenReturn(customerId);
        when(customerRepository.findByEmail(anyString())).thenReturn(Optional.empty());
        when(customerRepository.save(customer)).thenReturn(customer);

        UUID actualResponse = this.sut.createCustomer(customer, userId);

        verify(customer).setCreatedBy(userId);
        verify(customer).setCreationDateTime(any());
        verify(customerRepository).save(customer);

        assertEquals(customerId, actualResponse);
    }

    @Test
    void itShouldNotSaveCustomerIfTheEmailExists() {
        Customer customer = mock(Customer.class);
        String userId = "x-auth-user-id";
        UUID customerId = UUID.randomUUID();
        String email = "email@monkeys.com";

        when(customer.getEmail()).thenReturn(email);
        when(customer.getId()).thenReturn(customerId);
        when(customerRepository.findByEmail(customer.getEmail())).thenReturn(Optional.of(customer));

        Exception exception = assertThrows(Exception.class, () -> this.sut.createCustomer(customer, userId));

        verify(customerRepository, never()).save(any());

        assertEquals(BusinessException.class, exception.getClass());
        assertEquals(String.format(EMAIL_ALREADY_REGISTERED_ERROR_MESSAGE, email), exception.getMessage());

    }

    @Test
    void itShouldUpdateCustomerIfCustomerExists() {
        Customer customer = Customer.builder().id(UUID.randomUUID()).build();
        Customer oldCustomer = Customer.builder().id(UUID.randomUUID()).build();
        String userId = "x-auth-user-id";

        when(customerRepository.findById(customer.getId())).thenReturn(Optional.of(oldCustomer));

        this.sut.updateCustomer(customer, userId);

        assertEquals(userId, oldCustomer.getModifiedBy());
        verify(customerRepository).findById(customer.getId());
        verify(customerRepository).save(oldCustomer);
    }

    @Test
    void itShouldThrowNotFoundExceptionIfTheCustomerToUpdateIsNotFound() {
        Customer customer = Customer.builder().id(UUID.randomUUID()).build();
        String userId = "x-auth-user-id";

        when(customerRepository.findById(customer.getId())).thenReturn(Optional.empty());

        Exception exception = assertThrows(Exception.class, () -> this.sut.updateCustomer(customer, userId));

        verify(customerRepository).findById(customer.getId());

        assertEquals(EntityNotFoundException.class, exception.getClass());
        assertEquals(String.format(CUSTOMER_NOT_FOUND_ERROR_MESSAGE, customer.getId()), exception.getMessage());
    }

    @Test
    void itShouldDeleteCustomerIfCustomerExists() {
        UUID customerId = UUID.randomUUID();
        Customer customer = mock(Customer.class);

        when(customerRepository.findById(customerId)).thenReturn(Optional.of(customer));

        this.sut.deleteCustomer(customerId);

        verify(customerRepository).findById(customerId);
        verify(customerRepository).delete(customer);
    }

    @Test
    void itShouldThrowNotFoundExceptionIfTheCustomerToDeleteIsNotFound() {

        UUID customerId = UUID.randomUUID();
        when(customerRepository.findById(customerId)).thenReturn(Optional.empty());

        Exception exception = assertThrows(Exception.class, () -> this.sut.deleteCustomer(customerId));

        verify(customerRepository).findById(customerId);

        assertEquals(EntityNotFoundException.class, exception.getClass());
        assertEquals(String.format(CUSTOMER_NOT_FOUND_ERROR_MESSAGE, customerId), exception.getMessage());
    }


}