package com.tousled.theagilemonkeys.converters;

import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.models.dto.CustomerResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerToCustomerResponseConverter implements Converter<Customer, CustomerResponse> {

    @Override
    public CustomerResponse convert(Customer source) {
        return CustomerResponse.builder()
                .id(source.getId())
                .name(source.getName())
                .surname(source.getSurname())
                .email(source.getEmail())
                .createdBy(source.getCreatedBy())
                .modifiedBy(source.getModifiedBy())
                .build();
    }
}
