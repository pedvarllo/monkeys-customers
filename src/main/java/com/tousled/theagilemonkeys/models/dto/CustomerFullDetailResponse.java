package com.tousled.theagilemonkeys.models.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
public class CustomerFullDetailResponse {

    private UUID id;
    private String name;
    private String surname;
    private String photoPath;
    private String email;
    private String createdBy;
    private LocalDateTime creationDateTime;
    private String modifiedBy;
    private LocalDateTime modificationDateTime;

    @Tolerate
    public CustomerFullDetailResponse() {
    }
}
