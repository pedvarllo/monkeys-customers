package com.tousled.theagilemonkeys.converters;

import com.tousled.theagilemonkeys.CustomerObjectMother;
import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.models.dto.CustomerFullDetailResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CustomerToCustomerFullDetailResponseConverterTest {

    private CustomerToCustomerFullDetailResponseConverter cut;

    @BeforeEach
    void setUp() {
        this.cut = new CustomerToCustomerFullDetailResponseConverter();
    }

    @Test
    void itShouldConvertFromCustomerToCustomerFullDetailResponse() {

        Customer customer = CustomerObjectMother
                .createFullCustomer(UUID.randomUUID(), "Dua", "Lipa", "photo-url", "created-by", "dua.lipa@monkeys.com");

        CustomerFullDetailResponse expectedResponse = this.cut.convert(customer);

        assertNotNull(expectedResponse);
        assertEquals(customer.getId(), expectedResponse.getId());
        assertEquals(customer.getName(), expectedResponse.getName());
        assertEquals(customer.getSurname(), expectedResponse.getSurname());
        assertEquals(customer.getPhotoPath(), expectedResponse.getPhotoPath());
        assertEquals(customer.getEmail(), expectedResponse.getEmail());
        assertEquals(customer.getCreatedBy(), expectedResponse.getCreatedBy());
        assertEquals(customer.getModifiedBy(), expectedResponse.getModifiedBy());
        assertEquals(customer.getCreationDateTime(), expectedResponse.getCreationDateTime());
        assertEquals(customer.getModificationDateTime(), expectedResponse.getModificationDateTime());
    }
}