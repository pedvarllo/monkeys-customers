package com.tousled.theagilemonkeys.converters;

import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.models.dto.CustomerFullDetailResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerToCustomerFullDetailResponseConverter implements Converter<Customer, CustomerFullDetailResponse> {
    @Override
    public CustomerFullDetailResponse convert(Customer source) {
        return CustomerFullDetailResponse.builder()
                .id(source.getId())
                .name(source.getName())
                .surname(source.getSurname())
                .photoPath(source.getPhotoPath())
                .email(source.getEmail())
                .createdBy(source.getCreatedBy())
                .modifiedBy(source.getModifiedBy())
                .creationDateTime(source.getCreationDateTime())
                .modificationDateTime(source.getModificationDateTime())
                .build();
    }
}
