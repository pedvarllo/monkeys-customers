package com.tousled.theagilemonkeys.converters;

import com.tousled.theagilemonkeys.CustomerCommandObjectMother;
import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.models.dto.CustomerCommandRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomerCommandToCustomerConverterTestRequest {

    private CustomerCommandToCustomerConverter cut;

    @BeforeEach
    void setUp() {
        this.cut = new CustomerCommandToCustomerConverter();
    }

    @Test
    void itShouldConvertFromCustomerCommandToCustomer() {

        CustomerCommandRequest source = CustomerCommandObjectMother.createCustomerCommand("Frank", "Smith", "frank.smith@monkeys.com");

        Customer actualResponse = this.cut.convert(source);

        assertEquals(source.getName(), actualResponse.getName());
        assertEquals(source.getSurname(), actualResponse.getSurname());
        assertEquals(source.getEmail(), actualResponse.getEmail());
    }
}