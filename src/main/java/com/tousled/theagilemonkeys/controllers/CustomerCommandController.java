package com.tousled.theagilemonkeys.controllers;


import com.tousled.theagilemonkeys.exceptions.EntityNotFoundException;
import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.models.dto.CustomerCommandRequest;
import com.tousled.theagilemonkeys.models.dto.CustomerCommandResponse;
import com.tousled.theagilemonkeys.models.dto.CustomerUpdateCommand;
import com.tousled.theagilemonkeys.services.CustomerCommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerCommandController {

    private final CustomerCommandService customerCommandService;
    private final ConversionService conversionService;

    @Autowired
    public CustomerCommandController(CustomerCommandService customerCommandService,
                                     ConversionService conversionService) {
        this.customerCommandService = customerCommandService;
        this.conversionService = conversionService;
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<CustomerCommandResponse> createCustomer(@RequestBody CustomerCommandRequest customerCommandRequest,
                                                                  @RequestHeader("x-auth-user-id") String userId) {
        Customer customer = this.conversionService.convert(customerCommandRequest, Customer.class);
        UUID customerId = this.customerCommandService.createCustomer(customer, userId);
        CustomerCommandResponse response = CustomerCommandResponse.builder().customerId(customerId).build();
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Void> updateCustomer(@RequestBody CustomerUpdateCommand customerCommand,
                                               @RequestHeader("x-auth-user-id") String userId) throws EntityNotFoundException {
        Customer customer = this.conversionService.convert(customerCommand, Customer.class);
        this.customerCommandService.updateCustomer(customer, userId);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteCustomer(@PathVariable("id") UUID id) throws EntityNotFoundException {

        this.customerCommandService.deleteCustomer(id);
        return ResponseEntity.noContent().build();
    }
}
