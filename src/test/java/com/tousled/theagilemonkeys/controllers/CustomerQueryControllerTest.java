package com.tousled.theagilemonkeys.controllers;

import com.tousled.theagilemonkeys.CustomerObjectMother;
import com.tousled.theagilemonkeys.CustomerResponseObjectMother;
import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.models.dto.CustomerFullDetailResponse;
import com.tousled.theagilemonkeys.models.dto.CustomerResponse;
import com.tousled.theagilemonkeys.services.CustomerQueryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class CustomerQueryControllerTest {

    private CustomerQueryService customerQueryService;
    private ConversionService conversionService;
    private CustomerQueryController cut;

    @BeforeEach
    void setUp() {
        this.customerQueryService = mock(CustomerQueryService.class);
        this.conversionService = mock(ConversionService.class);
        this.cut = new CustomerQueryController(customerQueryService, conversionService);
    }

    @Test
    void itShouldGetAllCustomers() {

        List<Customer> retrievedCustomers = CustomerObjectMother.createCustomerList();
        List<CustomerResponse> expectedResponse = CustomerResponseObjectMother.createCustomerResponseList();

        when(customerQueryService.getAllCustomers()).thenReturn(retrievedCustomers);

        retrievedCustomers
                .forEach(c -> when(conversionService.convert(c, CustomerResponse.class))
                        .thenReturn(CustomerResponseObjectMother.createCustomerResponseFromCustomer(c)));

        ResponseEntity<List<CustomerResponse>> actualResponse = this.cut.getAllCustomers();

        retrievedCustomers.forEach(c -> verify(conversionService).convert(c, CustomerResponse.class));
        verify(customerQueryService).getAllCustomers();
        assertEquals(HttpStatus.OK, actualResponse.getStatusCode());
        assertEquals(expectedResponse, actualResponse.getBody());
    }

    @Test
    void itShouldGetTheCustomerWithIdGiven() {
        UUID id = UUID.randomUUID();
        Customer customer = CustomerObjectMother
                .createCustomer(id, "Dua", "Lipa", "photo-url", "created-by", "dua.lipa@monkeys.com");
        CustomerFullDetailResponse customerResponse = CustomerResponseObjectMother.createCustomerFullDetailResponseFromCustomer(customer);

        when(customerQueryService.getCustomerById(id)).thenReturn(customer);
        when(conversionService.convert(customer, CustomerFullDetailResponse.class)).thenReturn(customerResponse);

        ResponseEntity<CustomerFullDetailResponse> actualResponse = this.cut.getCustomerById(id);

        assertEquals(HttpStatus.OK, actualResponse.getStatusCode());
        assertEquals(customerResponse, actualResponse.getBody());


    }
}