package com.tousled.theagilemonkeys.events;

import com.tousled.theagilemonkeys.events.dto.DocumentEventPayload;
import com.tousled.theagilemonkeys.services.CustomerCommandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class KafkaConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumer.class);

    private final CustomerCommandService customerCommandService;

    @Autowired
    public KafkaConsumer(CustomerCommandService customerCommandService) {
        this.customerCommandService = customerCommandService;
    }

    @KafkaListener(topics = "${kafka.documents.topic.name}", groupId = "${spring.kafka.consumer.group-id}", containerFactory = "customersListener")
    public void consumeEvents(DocumentEventPayload documentEventPayload, Acknowledgment acknowledgment) {

        LOGGER.info("Event received");
        if (documentEventPayload != null) {
            LOGGER.info("DocumentEventPayload event received with body: {}", documentEventPayload);
            String documentPath = documentEventPayload.getDocumentPath();
            UUID ownerId = documentEventPayload.getOwnerId();
            String userId = documentEventPayload.getUserId();
            this.customerCommandService.updateCustomerDocument(documentPath, ownerId, userId);
        }

        LOGGER.info("Acknowledge");
        acknowledgment.acknowledge();
    }
}