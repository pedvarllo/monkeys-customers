package com.tousled.theagilemonkeys.converters;

import com.tousled.theagilemonkeys.models.Customer;
import com.tousled.theagilemonkeys.models.dto.CustomerCommandRequest;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerCommandToCustomerConverter implements Converter<CustomerCommandRequest, Customer> {

    @Override
    public Customer convert(CustomerCommandRequest source) {
        return Customer.builder()
                .name(source.getName())
                .surname(source.getSurname())
                .email(source.getEmail())
                .build();
    }
}
